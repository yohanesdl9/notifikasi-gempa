<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kota extends CI_Controller {

	public function __construct(){
		parent::__construct();
    }
    
    public function kota($provinsi){
        $query = 'SELECT K.nama_kota FROM kota AS K
            INNER JOIN provinsi AS P on K.id_provinsi = P.id_provinsi
            WHERE P.nama_provinsi = "' . str_replace('_', ' ', $provinsi) . '";';
        $kota = $this->db->query($query)->result();
        echo json_encode($kota);
    }
}
