<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notif_gempa extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->helper(array('datedb_helper', 'distance_helper'));
		$this->load->library('notification');
		$this->load->model('mod_gempa');
		$this->load->model('mod_user');
		$this->load->model('mod_notifikasi');
		$this->load->helper('distance_helper');
	}

	public function check(){
		$lat = -4.09;
		$lng = 122.5;
		echo '<pre>';
		print_r(checkLatLng(-7.96, 112.6));
		echo '</pre>';
	}
	
	public function index(){
		/* Setiap lima detik sekali, script ini akan dijalankan */
		copy('http://data.bmkg.go.id/gempadirasakan.xml', './datasource/gempadirasakan.xml');
		$array_gempa_dirasakan = simplexml_load_file(base_url('datasource/gempadirasakan.xml'));
		$data_gempa = $this->convert_to_array($array_gempa_dirasakan);
		$data_terbaru = array();
		$log_data = array();
for ($i = 0; $i < count($data_gempa); $i++){
	$latLng = explode(', ', $data_gempa[$i]['Koordinat']);
	$data = $this->mod_gempa->update_data($data_gempa[$i]['Tanggal'], $latLng[0], $latLng[1], $data_gempa[$i]['Magnitude'],
	$data_gempa[$i]['Kedalaman'], $data_gempa[$i]['Keterangan'], $data_gempa[$i]['Dirasakan']);
	if ($data > 0) { 
		array_push($data_terbaru, $data_gempa[$i]);
		array_push($log_data, $this->push_notification($data_gempa[$i])); 
	}
}
$this->load->view('info_gempa', compact('array_gempa_dirasakan', 'data_gempa', 'data_terbaru', 'log_data'));
	}

	function push_notification($dg){
		$notification_log = array();
		$users = $this->mod_user->getAllUser();
		foreach ($users as $u){
			$latLng = explode(', ', $dg['Koordinat']);
			$message = "Mag: " . $dg['Magnitude'] . " " . $dg['Tanggal'] . " Lok: " . $dg['Koordinat'] . " (" 
			. $dg['Keterangan'] . ") Kedalaman: " . $dg['Kedalaman'] . " km.";
			$distance = calc_distance($u->last_loc_x, $u->last_loc_y, $latLng[0], $latLng[1]);
			if (checkCoordinateDirasakan($u->last_loc_x, $u->last_loc_y, $dg['Dirasakan']) == '1'){
				if ($dg['Kedalaman'] <= 66 && $dg['Magnitude'] > 6.0 && dg['GempaLaut'] == true){
					$title = 'GEMPA BUMI DIRASAKAN >= 5.0 (BERPOTENSI TSUNAMI)';
				} else if ($dg['Magnitude'] >= 5.0){
					$title = 'GEMPA BUMI DIRASAKAN >= 5.0';
				} else {
					$title = 'GEMPA BUMI DIRASAKAN';
				}
				$this->mod_notifikasi->changeNotificationStatus($dg['Tanggal'], $u->email, $u->last_loc_x, $u->last_loc_y);
			} else {
				$title = 'INFO GEMPA';
			}
			$this->notification->setTitle($title);
			$this->notification->setMessage($message);
			$this->notification->setEmail($u->email);
			$this->notification->setLatitude($latLng[0]);
			$this->notification->setLongitude($latLng[1]);
			$this->notification->setKotaSetting($u->kota_setting);
			$requestData = $this->notification->getNotifications();
			array_push($requestData, ['Result' => $this->notification->pushNotification($u->token, $requestData)]);
			array_push($notification_log, $requestData);
		}
		return $notification_log;
	}

function convert_to_array($file_loaded){
	$data_gempa = array();
	foreach ($file_loaded as $gempa){
		$tanggal = explode('-', $gempa->Tanggal);
		$detail_gempa = array(
			'Tanggal' => parse_date_to_timestamp($tanggal[0], $tanggal[1]),
			'Koordinat' => $gempa->point->coordinates,
			'Magnitude' => $gempa->Magnitude,
			'Kedalaman' => explode (' ', $gempa->Kedalaman)[0],
			'Keterangan' => trim($gempa->Keterangan),
			'Dirasakan' => substr(trim($gempa->Dirasakan), 0, strlen(trim($gempa->Dirasakan)) - 1),
			'GempaLaut' => (stripos($gempa->Keterangan, 'laut') || strpos($gempa->Keterangan, 'dilaut') || strpos($gempa->Keterangan, 'diLaut'))
		);
		array_push($data_gempa, $detail_gempa);
	}
	uasort($data_gempa, function($a, $b){
		return strcmp($a['Tanggal'], $b['Tanggal']);
	});
	return $data_gempa;
}
}