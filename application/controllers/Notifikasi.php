<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notifikasi extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->model('mod_notifikasi');
    }

    public function changeStatus(){
        $email = $this->input->post('email');
        $datetime = $this->input->post('datetime');
        $status = $this->input->post('status');
        if ($this->input->post('user_loc_x') != 0 || $this->input->post('user_loc_y') != 0){
            $user_loc_x = $this->input->post('user_loc_x');
            $user_loc_y = $this->input->post('user_loc_y');
        } else {
            $user = $this->db->where('email', $email)->get('user')->row();
            $user_loc_x = $user->last_loc_x;
            $user_loc_y = $user->last_loc_y;
        }
        echo $this->mod_notifikasi->changeNotificationStatus($datetime, $email, $user_loc_x, $user_loc_y, $status);
    }

    public function putToken(){
        $firebase_token = $this->input->post('token');
        echo $this->mod_notifikasi->putToken($firebase_token);
    }

}