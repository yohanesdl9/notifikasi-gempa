<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {
  public function __construct(){
    parent::__construct();
    $this->load->model('mod_user');
  }

  public function getUser(){
    $email = $this->input->post('email');
    echo $this->mod_user->getUser($email);
  }

  public function registrasi(){
    $email = $this->input->post('email');
    $nama = $this->input->post('nama');
    $password = $this->input->post('password');
    echo $this->mod_user->registrasi($nama, $email, md5($password));
  }

  public function checkUser(){
    $email = $this->input->post('email');
    echo $this->mod_user->checkUser($email);
  }

  public function resetPassword(){
    $email = $this->input->post('email');
    $password = $this->input->post('password');
    echo $this->mod_user->resetPassword($email, md5($password));
  }

  public function login(){
    $email = $this->input->post('email');
    $password = $this->input->post('password');
    echo $this->mod_user->login($email, md5($password));
  }

  public function updateLocation(){
    $email = $this->input->post('email');
    $latitude = $this->input->post('latitude');
    $longitude = $this->input->post('longitude');
    echo $this->mod_user->updateLoc($email, $latitude, $longitude);
  }

  public function saveChanges(){
    $new_email = $this->input->post('new_email');
    $old_email = $this->input->post('old_email') ? $this->input->post('old_email') : $new_email;
    $nama = $this->input->post('name');
    $password = $this->input->post('password') ? $this->input->post('password') : "";
    $kota = $this->input->post('kota_setting');
    $prov = $this->input->post('prov_setting');
    echo $this->mod_user->saveSetting($old_email, $new_email, $nama, $password, $kota, $prov);
  }

  public function setSession(){
    $token = $this->input->post('token');
    $email = $this->input->post('email');
    echo $this->mod_user->setSession($token, $email);
  }

  public function logout(){
    $email = $this->input->post('email');
    echo $this->mod_user->clearSession($email);
  }
}