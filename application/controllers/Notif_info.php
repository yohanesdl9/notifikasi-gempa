<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notif_info extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->helper(array('datedb_helper', 'distance_helper'));
		$this->load->library('notification');
		$this->load->model('mod_gempa');
		$this->load->model('mod_user');
		$this->load->helper('distance_helper');
	}
	
	public function index(){

        $notification_log = array();
        $users = $this->mod_user->getAllDevice();
        $dg = $this->mod_gempa->getLastGempa();
        foreach ($users as $u){
            $title = 'GEMPA BUMI TERKINI';
            $message = "Mag: " . $dg->magnitude . " " . $dg->tanggal . " Lok: " . $dg->latitude . ", " . $dg->longitude . " (" . $dg->daerah_pusat_gempa . ") Kedalaman: " . $dg->kedalaman . " km.";
            $this->notification->setTitle($title);
            $this->notification->setMessage($message);
            $requestData = $this->notification->getNotifications();
            array_push($requestData, $this->notification->pushNotification($u->token, $requestData));
            array_push($notification_log, $requestData);
        }
        echo '<pre>';
        print_r($notification_log);
        echo '</pre>';
	}
}