<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Info_gempa extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->helper(array('datedb_helper', 'distance_helper'));
		$this->load->library(array('template', 'pagination'));
		$this->load->model('mod_gempa');
	}

	public function gempa_terkini(){
		echo json_encode($this->mod_gempa->getGempaTerkini());
	}
	
	public function index($page = null){
		$filter_id = $this->input->post('filterId') ? $this->input->post('filterId') : 1;
		if ($page == null){
			$offset = 0;
		} else {
			$offset = ($page * 10) - 10;
		}
		$data_gempa = $this->mod_gempa->getAllGempa(10, $offset, $filter_id);
		$config = [
			'base_url' => base_url('info_gempa/index'),
			'uri_segment' => 3,
			'per_page' => 10,
			'total_rows' => $this->mod_gempa->getAllGempa_2($filter_id)->num_rows(),
			'use_page_numbers' => true,
			'num_links' => 3,
			'prev_link' => 'Sebelumnya',
			'next_link' => 'Berikutnya',
			'first_link' => 'Terbaru',
			'last_link' => 'Terlama',
			'full_tag_open' => '<ul class="pagination">',
			'full_tag_close' => '</ul>',
			'prev_tag_open' => '<li class="page-item"> <span class="page-link">',
			'prev_tag_close' => '</span> </li>',
			'next_tag_open' => '<li class="page-item"> <span class="page-link">',
			'next_tag_close' => '</span> </li>',
			'first_tag_open' => '<li class="page-item"> <span class="page-link">',
			'first_tag_close' => '</span> </li>',
			'last_tag_open' => '<li class="page-item"> <span class="page-link">',
			'last_tag_close' => '</span> </li>',
			'num_tag_open' => '<li class="page-item"> <span class="page-link">',
			'num_tag_close' => '</span> </li>',
			'cur_tag_open' => '<li class="page-item active"> <span class="page-link">',
			'cur_tag_close' => '</span> </li>'
		];
		$this->pagination->initialize($config);
		$pagination = $this->pagination->create_links();
		$data = [
			'title' => 'Gempa Bumi Terkini - Info Gempa',
			'pagination' => $pagination,
			'data_gempa' => $data_gempa,
			'filter_id' => $filter_id
		];
        $this->template->load('default', 'dashboard', $data);
	}

	public function sebaran_gempa(){
		$data_gempa = $this->mod_gempa->getAllGempa_2($filter_id)->result();
		$data = [
			'title' => 'Gempa Bumi Terkini - Info Gempa',
			'data_gempa' => $data_gempa,
			'filter_id' => $filter_id,
			'display_id' => $display_id
		];
		$this->template->load('default', 'sebaran_gempa', $data);
	}

	public function detail_public($tanggal){
		$tanggal = str_replace('_', ' ', $tanggal);
		$detail_gempa = $this->mod_gempa->getDetailGempa($tanggal);
		$data = [
			'title' => 'Info Gempa - ' . tanggal($tanggal),
			'detail' => $detail_gempa
		];
		$this->template->load('default', 'detail_public', $data);
	}

	public function detail($tanggal){
		$search_mail = $this->input->post('search_mail');
		$tanggalku = str_replace('_', ' ', $tanggal);
		$detail_gempa = $this->mod_gempa->getDetailGempa($tanggalku);
		$users = $this->mod_gempa->getUserNotified($tanggalku, $search_mail);
		if (count($users) > 0){
			$data = [
				'title' => 'Info Gempa - ' . tanggal($tanggalku),
				'detail' => $detail_gempa,
				'users' => $users
			];
			$this->template->load('default', 'detail', $data);
		} else {
			$this->session->set_flashdata('empty', 'Email yang dimasukkan tidak terdaftar di aplikasi, atau rekan Anda tidak berada dekat pusat gempa saat gempa terjadi.');
			redirect('info_gempa/detail_public/' . $tanggal);
		}
	}
}