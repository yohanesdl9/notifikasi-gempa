<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct(){
        parent::__construct();
        $this->load->library('template');
	}

	public function index(){
        $data['title'] = 'Login Admin';
        $this->template->load('default', 'login', $data);
	}

	public function auth_admin(){
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        if ($username == 'admin' && $password == 'superadmin123'){
            $this->session->set_userdata(array('isAdmin' => true));
            redirect('info_gempa');
        } else {
            $data['title'] = 'Login Admin - Error';
            $this->session->set_flashdata('error_message', 'Username dan password salah. Silahkan coba lagi');
            $this->template->load('default', 'login', $data);
        }
    }

    public function admin_logout(){
        $this->session->sess_destroy();
        redirect('info_gempa');
    }
}