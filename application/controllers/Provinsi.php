<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Provinsi extends CI_Controller {

	public function __construct(){
		parent::__construct();
    }
    
    public function index(){
        $kota = $this->db->get('provinsi')->result();
        echo json_encode($kota);
    }
}
