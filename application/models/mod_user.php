<?php
class mod_user extends CI_Model{
    public function registrasi($nama, $email, $password){
        $data = [
            'email' => $email,
            'nama' => $nama,
            'password' => $password
        ];
        $this->db->insert('user', $data);
        return $this->db->affected_rows();
    }

    public function getAllDevice(){
        return $this->db->get('firebase_token')->result();
    }

    public function login($email, $password){
        $where = ['email' => $email, 'password' => $password];
        return $this->db->where($where)->get('user')->num_rows();
    }

    public function updateLoc($email, $latitude, $longitude){
        $data = ['last_loc_x' => $latitude, 'last_loc_y' => $longitude];
        $this->db->update('user', $data, ['email' => $email]);
        return $latitude . ', ' . $longitude . ', ' . $email;
    }

    public function resetPassword($email, $password){
        $data = ['password' => $password];
        $this->db->update('user', $data, ['email' => $email]);
        return $this->db->affected_rows();
    }

    public function checkUser($email){
        return $this->db->where('email', $email)->get('user')->num_rows();
    }

    public function getUser($email){
        $data_user = $this->db->where('email', $email)->get('user')->result();
        return json_encode($data_user);
    }

    public function saveSetting($old_email, $new_email, $name, $password, $kota, $provinsi){
        $data = ['nama' => $name, 'kota_setting' => $kota, 'prov_setting' => $provinsi];
        if ($old_email != $new_email){
            $data['email'] = $new_email;
        }
        if (!empty($password)){
            $data['password'] = md5($password);
        }
        $this->db->update('user', $data, ['email' => $old_email]);
        echo $this->db->affected_rows();
    }

    public function getAllUser(){
        $query = 'SELECT S.token, U.email, U.nama, U.last_loc_y, U.last_loc_x, U.kota_setting
        FROM `session` AS S INNER JOIN `user` AS U ON S.email = U.email;';
        return $this->db->query($query)->result();
    }

    public function setSession($token, $email){
        $data = ['token' => $token, 'email' => $email];
        $this->db->insert('session', $data);
        return $this->db->affected_rows();
    }

    public function clearSession($email){
        $this->db->delete('session', array('email' => $email));
        return $this->db->affected_rows();
    }
}
?>