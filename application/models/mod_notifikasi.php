<?php
class mod_notifikasi extends CI_Model{

    public function putToken($token){
        if ($this->db->where('token', $token)->get('firebase_token')->num_rows() == 0){
            $data = ['token' => $token];
            $this->db->insert('firebase_token', $data);
            return $this->db->affected_rows();
        } else {
            return 0;
        }
    }

    public function changeNotificationStatus($datetime, $email, $user_loc_x, $user_loc_y, $status = 'NOTIFIED'){
        $where = ['email' => $email, 'datetime_gempa' => $datetime];
        $is_notified = $this->db->where($where)->get('detail')->num_rows();
        if ($is_notified > 0){
            $data = ['konfirmasi' => $status, 'user_loc_x' => $user_loc_x, 'user_loc_y' => $user_loc_y];
            $this->db->update('detail', $data, $where);
            echo 'UPDATED';
        } else {
            $data = ['datetime_gempa' => $datetime, 'email' => $email, 'konfirmasi' => $status, 
                'user_loc_x' => $user_loc_x, 'user_loc_y' => $user_loc_y];
            $this->db->insert('detail', $data);
            echo 'NOTIFIED';
        }
    }
}
?>