<?php
class mod_gempa extends CI_Model{
    public function getLastGempa(){
        return $this->db->limit(1)->order_by('tanggal', 'DESC')->get('gempa_terbaru')->row();
    }

    public function getGempaTerkini(){
        $this->db->order_by('tanggal', 'DESC')->limit(1, 0);
        return $this->db->get('gempa_terbaru')->result();
    }

    public function getAllGempa($per_page, $offset, $filter_id = 1){
        switch ($filter_id){
            case 2:
                $this->db->where('magnitude >=', 5.0);
                break;
            case 3:
                $this->db->where('letak_pusat_gempa', 'laut')->where('kedalaman <', 60)->where('magnitude >=', 6.0);
                break;
        }
        return $this->db->order_by('tanggal', 'DESC')->limit($per_page, $offset)->get('gempa_terbaru')->result();
    }

    public function getAllGempa_2($filter_id = 1){
        switch ($filter_id){
            case 2:
                $this->db->where('magnitude >=', 5.0);
                break;
            case 3:
                $this->db->where('letak_pusat_gempa', 'laut')->where('kedalaman <', 60)->where('magnitude >=', 6.0);
                break;
        }
        return $this->db->order_by('tanggal', 'DESC')->get('gempa_terbaru');
    }

    public function getUserNotified($tanggalGempa, $search_mail = ''){
        $this->db->select('gempa_terbaru.tanggal, detail.email, user.nama, detail.konfirmasi, detail.user_loc_x, detail.user_loc_y');
        $this->db->join('gempa_terbaru', 'detail.datetime_gempa = gempa_terbaru.tanggal', 'inner');
        $this->db->join('user', 'detail.email = user.email', 'inner');
        $this->db->where('gempa_terbaru.tanggal', $tanggalGempa);
        if (!empty($search_mail)){
            $this->db->where('detail.email', $search_mail);
        }
        return $this->db->get('detail')->result();
    }

    public function getDetailGempa($tanggalGempa){
        return $this->db->where('tanggal', $tanggalGempa)->get('gempa_terbaru')->row();
    }

    public function update_data($tanggal, $lat, $long, $mag, $kdlmn, $daerah, $dirasakan){
        $where = array(
            'tanggal' => $tanggal,
            'latitude' => $lat, 
            'longitude' => $long, 
            'magnitude' => $mag, 
            'kedalaman' => $kdlmn, 
            'wilayah' => $daerah, 
            'dirasakan' => $dirasakan
        );
        $check_gempa_exist = $this->db->where($where)->get('gempa_terbaru');
        if ($check_gempa_exist->num_rows() == 0){
            $this->db->insert('gempa_terbaru', $where);
            return 1;
        } else {
            return 0;
        }
    }
}
?>