<?php
class Notification{
	private $title;
	private $message;
	private $email;
	private $latitude;
	private $longitude;
	private $kota_set;
	private $data;
 
	public function setTitle($title){
		$this->title = $title;
	}
 
	public function setMessage($message){
		$this->message = $message;
	}

	public function setEmail($email){
		$this->email = $email;
	}

	public function setLatitude($latitude){
		$this->latitude = $latitude;
	}

	public function setLongitude($longitude){
		$this->longitude = $longitude;
	}

	public function setKotaSetting($kota_set){
		$this->kota_set = $kota_set;
	}
	
public function getNotifications(){
	$notification = array();
	$notification['title'] = $this->title;
	$notification['body'] = $this->message;
	if ($notification['title'] != 'GEMPA BUMI TERKINI'){
		$notification['email'] = $this->email;
		$notification['latitude'] = $this->latitude;
		$notification['longitude'] = $this->longitude;
		$notification['set_kota'] = $this->kota_set;
	}
	return $notification;
}

public function pushNotification($firebase_token, $requestData){
	$fields = array(
		'to' => $firebase_token,
		'data' => $requestData
	);
	$url = 'https://fcm.googleapis.com/fcm/send';
	$headers = array(
		'Authorization: key=AAAAyc0vQu8:APA91bGIJLHD9pzYeJhHsgy6ZXl4UQuE-iTUdJDy7Bm6GBoCIrMFFV2KeLc8cDTg4lnhwgavqObGmMFPHUnwHtdwYlE-UjzTKN8PrsQT5J43ofzeRqcBc9Yco3zjb1ZMGgKYwFzmyoLn',
		'Content-Type: application/json'
	);
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
	$result = curl_exec($ch);
	if($result === FALSE){
		die('Curl failed: ' . curl_error($ch));
	}
	curl_close($ch);
	return $result;
}
}
?>