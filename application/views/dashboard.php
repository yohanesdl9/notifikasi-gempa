<div class="py-2">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h1 class="display-4">Gempa Bumi Terkini</h1>
      </div>
    </div>
  </div>
</div>
<div class="py-2">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <form class="form-inline" action="" method="post" onchange="submit()" style="padding-bottom: 10px">
          <div class="form-group">
              <label>Filter Data Gempa Bumi</label>
              <div class="col-md-3">
                <select name="filterId" class="form-control select2">
                  <option value="1" <?php echo $filter_id == 1 ? 'selected' : '' ?>>Semua</option>
                  <option value="2" <?php echo $filter_id == 2 ? 'selected' : '' ?>>Magnitude > 5.0</option>
                  <option value="3" <?php echo $filter_id == 3 ? 'selected' : '' ?>>Berpotensi Tsunami</option>
                </select>
              </div>
          </div>
        </form>
        <div class="table-responsive">
          <table class="table table-bordered ">
            <thead class="thead-dark">
              <tr class="text-center">
                <th style="width: 15%">Tanggal &amp; Waktu</th>
                <th style="width: 15%">Lintang, Bujur</th>
                <th style="width: 5%">Magnitude</th>
                <th style="width: 5%">Kedalaman (km)</th>
                <th style="width: 35%">Wilayah</th>
                <th style="width: 5%">Action</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach($data_gempa as $dg) { ?>
              <tr>
                <td><?php echo tanggal($dg->tanggal); ?></td>
                <td><?php 
                  echo abs($dg->latitude) . ' ' . ($dg->latitude >= 0 ? 'LU' : 'LS');
                  echo ', ' . abs($dg->longitude) . ' ' . ($dg->longitude >= 0 ? 'BT' : 'BB');
                ?></td>
                <td class="text-center"><?php echo $dg->magnitude ?></td>
                <td class="text-center"><?php echo $dg->kedalaman ?></td>
                <td><?php echo $dg->wilayah ?></td>
                <td>
                  <a class="btn btn-sm btn-info" href="<?php
                    if ($this->session->userdata('isAdmin') == true){
                      echo base_url() . "info_gempa/detail/" . str_replace(' ', '_', $dg->tanggal);
                    } else {
                      echo base_url() . "info_gempa/detail_public/" . str_replace(' ', '_', $dg->tanggal);
                    }
                  ?>">Detail</a>
                </td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <?php echo $pagination; ?>
  </div>
</div>
<script>
function initMap() {
  var locations = [
    <?php foreach ($data_gempa as $dg){
      echo "['" . tanggal($dg->tanggal) . "', " . $dg->magnitude . ", " . $dg->kedalaman . ", '" . trim($dg->daerah_pusat_gempa) . "', " . $dg->latitude . ", " . $dg->longitude . "], ";
    } ?>
  ];
  var uluru = new google.maps.LatLng(0, 118);
  var map = new google.maps.Map(document.getElementById('big_map'), {zoom: 5, center: uluru});
  var infowindow = new google.maps.InfoWindow();

  var marker, i;

  for (i = 0; i < locations.length; i++) {  
    marker = new google.maps.Marker({
      position: new google.maps.LatLng(locations[i][4], locations[i][5]),
      map: map,
      icon: 'http://labs.google.com/ridefinder/images/mm_20_red.png'
    });

    google.maps.event.addListener(marker, 'click', (function(marker, i) {
      return function() {
        infowindow.setContent(locations[i][0] + '<br/>Mag : ' + locations[i][1] + '<br/>Kedalaman : ' + locations[i][2] + ' km. <br/>' + locations[i][3]);
        infowindow.open(map, marker);
      }
    })(marker, i));
  }
}
</script>