<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title><?php echo $title; ?></title>
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/font-awesome-4.7.0/css/font-awesome.min.css" type="text/css">
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/bootstrap-4.3.1.css">
</head>

<body>
  <nav class="navbar bg-primary navbar-dark">
    <div class="container"> 
      <a class="navbar-brand" href="<?php echo base_url('info_gempa') ?>">
        <i class="fa d-inline fa-lg fa-globe"></i>
        <b> Info Gempa</b></a>
        <?php if ($this->session->userdata('isAdmin') == true){ ?>
        <a class="btn btn-outline-light btn-pull-right navbar-btn ml-md-2" href="<?php echo base_url('admin/admin_logout') ?>">Logout</a>
        <?php } ?>
    </div>
  </nav>
  <?php echo $body; ?>
  <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCGoaNMdV8hvihfIs__lDLAtJMmuNv_dls&callback=initMap&sensor=false"></script>  
  <script src="<?php echo base_url() ?>assets/jquery-3.3.1.min.js"></script>
  <script src="<?php echo base_url() ?>assets/popper.min.js"></script>
  <script src="<?php echo base_url() ?>assets/bootstrap.min.js"></script>
</body>
</html>