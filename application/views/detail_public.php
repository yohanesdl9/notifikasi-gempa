<?php if ($this->session->flashdata('empty')){ ?>
<div class="py-4">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-danger" role="alert">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <h4 class="alert-heading">Oh snap!</h4>
                    <p class="mb-0"><?php echo $this->session->flashdata('empty'); ?></p>
                </div>
            </div>
        </div>
    </div>
</div>
<?php } ?>
<div class="py-2">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="display-4">Detail Lokasi Gempa & Pengguna</h1>
            </div>
        </div>
    </div>
</div>
<div class="py-2">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="table-responsive">
                <table class="table">
                    <tbody>
                    <tr>
                        <td>Tanggal &amp; Waktu</td>
                        <td><?php echo tanggal($detail->tanggal) ?></td>
                    </tr>
                    <tr>
                        <td>Koordinat</td>
                        <td><?php 
                    echo abs($detail->latitude) . ' ' . ($detail->latitude >= 0 ? 'LU' : 'LS');
                    echo ', ' . abs($detail->longitude) . ' ' . ($detail->longitude >= 0 ? 'BT' : 'BB'); ?></td>
                    </tr>
                    <tr>
                        <td>Magnitude</td>
                        <td><?php echo $detail->magnitude ?></td>
                    </tr>
                    <tr>
                        <td>Kedalaman</td>
                        <td><?php echo $detail->kedalaman . " km" ?></td>
                    </tr>
                    <tr>
                        <td>Letak Pusat Gempa</td>
                        <td><?php echo $detail->wilayah ?></td>
                    </tr>
                    </tbody>
                </table>
                </div>
            </div>
            <div class="col-md-8" id="map"></div>
        </div>
    </div>
</div>
<div class="py-4">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
            <form action="<?php echo base_url('info_gempa/detail/' . str_replace(' ', '_', $detail->tanggal)) ?>" method="post">
                <div class="form-group"> 
                    <label>Masukkan email rekanan Anda yang menggunakan aplikasi Info Gempa</label> 
                    <input type="email" class="form-control" placeholder="Masukkan Email" name="search_mail"> 
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
            </div>
        </div>
    </div>
</div>
<script>
function initMap() {
    var uluru = {lat: <?php echo $detail->latitude; ?>, lng: <?php echo $detail->longitude; ?>};
    var map = new google.maps.Map(document.getElementById('map'), {zoom: 10, center: uluru});
    var marker = new google.maps.Marker({
        position: new google.maps.LatLng(<?php echo $detail->latitude . ", " . $detail->longitude; ?>),
        map: map
    });
}
</script>