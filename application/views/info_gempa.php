<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <!-- <meta http-equiv="refresh" content="3"> -->
    <title>Informasi Gempa</title>
    <style>
    table, th, td {
        border: 1px solid black;
        border-collapse: collapse;
    }
</style>
</head>
<body>
    <p>Text mining dilakukan pukul : <?php echo date('d-M-Y H:i:s') ?></p>
    <h2>DATA ASLI</h2>
    <pre class="prettyprint linenums">
    <code class="language-xml"><?php echo htmlspecialchars(file_get_contents(base_url('datasource/gempadirasakan.xml')), ENT_QUOTES); ?></code>
    </pre>
    <h2>DATA SETELAH DIKONVERSI (BENTUK TABEL)</h2>
    <table>
        <thead>
            <tr>
                <th>Tanggal</th>
                <th>Koordinat</th>
                <th>Magnitude</th>
                <th>Kedalaman</th>
                <th>Keterangan</th>
                <th>Dirasakan</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($data_gempa as $d){ ?>
            <tr>
                <td><?php echo $d['Tanggal'] ?></td>
                <td><?php echo $d['Koordinat'] ?></td>
                <td><?php echo $d['Magnitude'] ?></td>
                <td><?php echo $d['Kedalaman'] ?></td>
                <td><?php echo $d['Keterangan'] ?></td>
                <td><?php echo $d['Dirasakan'] ?></td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
    <h2>GEMPA BUMI TERKINI</h2>
    <?php if (count($data_terbaru) == 0){
        echo "Tidak ada data terbaru saat ini.";
    } else { ?>
    <table>
        <thead>
            <tr>
                <th>Tanggal</th>
                <th>Koordinat</th>
                <th>Magnitude</th>
                <th>Kedalaman</th>
                <th>Keterangan</th>
                <th>Dirasakan</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($data_terbaru as $d){ ?>
            <tr>
                <td><?php echo $d['Tanggal'] ?></td>
                <td><?php echo $d['Koordinat'] ?></td>
                <td><?php echo $d['Magnitude'] ?></td>
                <td><?php echo $d['Kedalaman'] ?></td>
                <td><?php echo $d['Keterangan'] ?></td>
                <td><?php echo $d['Dirasakan'] ?></td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
    <h2>LOG DATA</h2>
    <pre><?php print_r($log_data); ?></pre>
    <?php } ?>
</body>
</html>