<div class="py-2">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="display-4">Detail Lokasi Gempa & Pengguna</h1>
            </div>
        </div>
    </div>
</div>
<div class="py-2">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="table-responsive">
                <table class="table">
                    <tbody>
                    <tr>
                        <td>Tanggal &amp; Waktu</td>
                        <td><?php echo tanggal($detail->tanggal) ?></td>
                    </tr>
                    <tr>
                        <td>Koordinat</td>
                        <td><?php 
                    echo abs($detail->latitude) . ' ' . ($detail->latitude >= 0 ? 'LU' : 'LS');
                    echo ', ' . abs($detail->longitude) . ' ' . ($detail->longitude >= 0 ? 'BT' : 'BB'); ?></td>
                    </tr>
                    <tr>
                        <td>Magnitude</td>
                        <td><?php echo $detail->magnitude ?></td>
                    </tr>
                    <tr>
                        <td>Kedalaman</td>
                        <td><?php echo $detail->kedalaman . " km" ?></td>
                    </tr>
                    <tr>
                        <td>Letak Pusat Gempa</td>
                        <td><?php echo $detail->wilayah ?></td>
                    </tr>
                    </tbody>
                </table>
                </div>
            </div>
            <div class="col-md-8" id="map"></div>
        </div>
    </div>
</div>
<div class="py-2"> 
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <form class="form-inline" style="padding-bottom: 10px">
                    <div class="form-group">
                        <label>Pilih Status Konfirmasi</label>
                        <div class="col-md-3">
                            <select id="selectKonfirmasi" onchange="filterTable()" class="form-control select2">
                                <option value="" selected="selected">Semua</option>
                                <option value="AMAN">Aman</option>
                                <option value="DALAM EVAKUASI">Dalam Evakuasi</option>
                                <option value="BELUM KONFIRMASI">Belum Konfirmasi</option>
                            </select>
                        </div>
                    </div>
                </form>
                <div class="table-responsive">
                    <table class="table table-bordered" id="tabelKonfirmasi">
                        <thead>
                            <tr>
                                <th>Nama Pengguna</th>
                                <th>Email</th>
                                <th>Status Konfirmasi</th>
                                <th>Jarak dari Pusat Gempa (km)</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($users as $u){ ?>
                                <tr>
                                    <td><?php echo $u->nama ?></td>
                                    <td><?php echo $u->email ?></td>
                                    <td><?php switch ($u->konfirmasi){
                                        case 'SAFE':
                                            echo 'AMAN';
                                            break;
                                        case 'EVACUATED':
                                            echo 'DALAM EVAKUASI';
                                            break;
                                        default:
                                            echo 'BELUM KONFIRMASI';
                                            break;
                                    } ?></td>
                                    <td>
                                        <?php echo number_format(calc_distance($u->user_loc_x, $u->user_loc_y, $detail->latitude, $detail->longitude), 4); ?>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>
<script>
function initMap() {
    var locations = [
        ['Lokasi Gempa', <?php echo $detail->latitude . ", " . $detail->longitude; ?>],
        <?php foreach ($users as $u){
            echo "['" . $u->nama . "', " . $u->user_loc_x . ", " . $u->user_loc_y . ", '";
            switch ($u->konfirmasi){
                case 'SAFE':
                    echo 'http://labs.google.com/ridefinder/images/mm_20_green.png';
                    break;
                case 'EVACUATED':
                    echo 'http://labs.google.com/ridefinder/images/mm_20_yellow.png';
                    break;
                default:
                    echo 'http://labs.google.com/ridefinder/images/mm_20_red.png';
                    break;
            }
            echo "'], ";
        } ?>
    ];
    var uluru = {lat: <?php echo $detail->latitude; ?>, lng: <?php echo $detail->longitude; ?>};
    var map = new google.maps.Map(document.getElementById('map'), {zoom: 11, center: uluru});
    var infowindow = new google.maps.InfoWindow();

    var marker, i;

    for (i = 0; i < locations.length; i++) {  
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        map: map,
        icon: locations[i][3]
      });

      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          infowindow.setContent(locations[i][0]);
          infowindow.open(map, marker);
        }
      })(marker, i));
    }
}
function filterTable(){
    var input = document.getElementById('selectKonfirmasi');
    var value = input.value;
    table = document.getElementById("tabelKonfirmasi");
    tr = table.getElementsByTagName("tr");
    if (value != ""){
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[2];
            if (td) {
                txtValue = td.textContent || td.innerText;
                if (txtValue.toUpperCase().indexOf(value) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }       
        }
    } else {
        for (i = 0; i < tr.length; i++) {
            tr[i].style.display = "";
        }
    }
}
</script>