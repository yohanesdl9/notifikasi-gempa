<div class="py-4 text-center">
    <div class="container">
        <div class="mx-auto col-md-6 col-10 p-4 bg-light">
        <?php if ($this->session->flashdata('error_message')){ ?>
        <div class="alert alert-danger" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <p class="mb-0"><?php echo $this->session->flashdata('error_message'); ?></p>
        </div>
        <?php } ?>
        <h1 class="mb-4">Login Admin</h1>
        <form action="<?php echo base_url() ?>admin/auth_admin" method="post">
            <div class="form-group"> <input type="text" name="username" class="form-control" placeholder="Username"></div>
            <div class="form-group"> <input type="password" name="password" class="form-control" placeholder="Password"></div> 
            <button type="submit" class="btn btn-primary">Login</button>
        </form>
        </div>
    </div>
</div>