<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('calc_distance')){
    function calc_distance($lat1, $lon1, $lat2, $lon2) {
        $dLat = ($lat2 - $lat1) * M_PI / 180.0; 
        $dLon = ($lon2 - $lon1) * M_PI / 180.0; 
        // convert to radians 
        $lat1 = ($lat1) * M_PI / 180.0; 
        $lat2 = ($lat2) * M_PI / 180.0; 
        // apply formulae 
        $a = pow(sin($dLat / 2), 2) + pow(sin($dLon / 2), 2) * cos($lat1) * cos($lat2); 
        $rad = 6371; 
        $c = 2 * asin(sqrt($a)); 
        return $rad * $c; 
    }

function checkCoordinateDirasakan($latitude, $longitude, $dirasakan){
    $daerahUser = checkLatLng($latitude, $longitude);
    $daerahTerdampak = explode(',', $dirasakan);
    $daerah = array();
    for ($i = 0; $i < count($daerahTerdampak); $i++){
        array_push($daerah, preg_split('/\s+/', trim($daerahTerdampak[$i]), 2)[1]);
    }
    for ($i = 0; $i < count($daerah); $i++){
        for ($j = 0; $j < count($daerahUser); $j++){
            if (strpos($daerahUser[$j], $daerah[$i]) !== false){
                return '1';
            }
        }
    }
    return '0';
}

function checkLatLng($lat, $lng){
    $url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng='.$lat.','.$lng.'&sensor=false&key=AIzaSyASkl-RyPTuXr2S_zXZMWz2koGdkIu92ww';
    $json = @file_get_contents($url);
    $data = json_decode($json);
    $status = $data->status;
    $addresses = array();
    if($status=="OK") {
        for ($j=0; $j<count($data->results[0]->address_components) - 1; $j++) {
            array_push($addresses, $data->results[0]->address_components[$j]->long_name);
        }
    } else {
        echo 'Location Not Found';
    }
    return $addresses;
}
}
?>