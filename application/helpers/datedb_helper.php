<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('parse_date_to_timestamp')){
    function parse_date_to_timestamp($date, $time){
        $dates = explode('/', $date);
        $times = explode(' ', $time);
        $date = $dates[2] . '-' . $dates[1] . '-' . $dates[0];
        return $date . ' ' . $times[0];
    }
}

if (!function_exists('tanggal')){
    function tanggal($datetime){
        $datetimes = explode(' ', $datetime);
        $dates = explode('-', $datetimes[0]);
        switch ($dates[1]){
            case 1:
                $month = "Januari";
                break;
            case 2:
                $month = "Februari";
                break;
            case 3:
                $month = "Maret";
                break;
            case 4:
                $month = "April";
                break;
            case 5:
                $month = "Mei";
                break;
            case 6:
                $month = "Juni";
                break;
            case 7:
                $month = "Juli";
                break;
            case 8:
                $month = "Agustus";
                break;
            case 9:
                $month = "September";
                break;
            case 10:
                $month = "Oktober";
                break;
            case 11:
                $month = "November";
                break;
            case 12:
                $month = "Desember";
                break;
        }
        return $dates[2] . ' ' . $month . ' ' . $dates[0] . ' ' . $datetimes[1];
    }
}
?>